import { Component } from "react";

import InputMessage from "./input/InputMessage";
import OuputMessage from "./output/OutputMessage";

class ContentComponent extends Component {
    render(){
        return (
            <div>
                <InputMessage />
                <OuputMessage />
            </div>
        )
    }
}

export default ContentComponent;