import { Component } from "react"
import likeImg from '../../../asset/images/maxresdefault.jpg'

class OuputMessage extends Component {
    render() {
        return (
            <div className="container text-center">
                <div className="row mt-2">
                    <div className='col-12'>
                        <img src={likeImg} alt="like" width={150} style={{ borderRadius: "50%" }}></img>
                    </div>
                </div>
            </div>    
        )
    }
}

                export default OuputMessage;