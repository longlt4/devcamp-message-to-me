import { Component } from "react"


class InputMessage extends Component {
    inputClickHandle(param) {
        console.log(param.target.value)
    }
    onBtnClickHandle() {
        console.log("nut bam dc click")
    }
    render() {
        return (
            <div className="container mt-2">
                <div className="row">
                    <div className='col-12'>
                        <p className='form-label'>Chao mung den Devacamp</p>
                    </div>
                </div>
                <div className="row">
                    <div className='col-12'>
                        <input className='form-control' placeholder='nhap massage' onChange={this.inputClickHandle}></input>
                    </div>
                </div>
                <div className="row mt-2">
                    <div className='col-12'>
                        <button className='btn btn-success' onClick={this.onBtnClickHandle} >Gui Thong diep</button>
                    </div>
                </div>
            </div>
        )
    }
}

export default InputMessage;