import { Component } from "react";

import TitleText from "./text/TittleText"
import TitleImage from "./images/TitleImages";

class TitleComponent extends Component {
    render() {
        return (
            <div>
                < TitleText/>
                < TitleImage/>
            </div>
        )
    }
}

export default TitleComponent