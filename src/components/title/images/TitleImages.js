import{ Component } from "react"
import backgroundImg from '../../../asset/images/OIP.jpg';

class TitleImage extends Component {
    render() {
        return (
            <div className="row">
        <div className='col-12'>
          <img src={backgroundImg} alt="background"></img>
        </div>
      </div>
        )
    }
}

export default TitleImage;