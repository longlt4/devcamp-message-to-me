import { Component } from "react"

class TitleText extends Component {
    render() {
        return (
            <div className="container text-center">
                <div className="row">
                    <div className='col-12'>
                        <h1>Chao Mung</h1>
                    </div>
                </div>
            </div>
        )
    }
}

export default TitleText;